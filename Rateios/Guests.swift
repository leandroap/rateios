//
//  Guests.swift
//  Rateios
//
//  Created by Leandro Pereira on 08/11/14.
//  Copyright (c) 2014 FIAP iOS Projects. All rights reserved.
//

import UIKit

class Guests: PFObject, PFSubclassing {
   
    override class func initialize() {
        struct Static {
            static var onceToken : dispatch_once_t = 0;
        }
        dispatch_once(&Static.onceToken) {
            self.registerSubclass()
        }
    }
    
    class func parseClassName() -> String! {
        return "guests"
    }
    
    func rateioId() -> Apportionment! {
        var rateio:Apportionment?
        var parent = self["parent"] as! PFObject
        parent.fetchIfNeededInBackgroundWithBlock {
            (parent: PFObject!, error: NSError!) -> Void in
            rateio = parent as! Apportionment!
            // do something with your title variable
        }
        return rateio
    }
    
    func facebookId() -> String {
        return self["facebookId"] as! String
    }
    
    func statusPgto() -> Bool {
        return self["paid"] as! Bool
    }
    
    func valorIndividual() -> Double {
        return self["value"] as! Double
    }
    
    class func findByFacebookId() -> Array<Rateio>! {
        //let predicate = NSPredicate(format:"userId = '\(userId)'")
        //var query = PFQuery(className: "apportionment", predicate:predicate)
        
        var rateios:Array<Rateio> = Array<Rateio>()
        
        let query:PFQuery = self.query()
        query.whereKey("userId", equalTo: PFUser.currentUser().objectId)
        
        query.findObjectsInBackgroundWithBlock {
            (objects: [AnyObject]!, error: NSError!) -> Void in
            if error == nil {
                // The find succeeded.
                NSLog("Rateios retrieved: \(objects.count)")
                // Do something with the found objects
                for object in objects {
                    NSLog("%@", object.objectId)
                    let rateio:Rateio = Rateio(apportionment: object as! Apportionment)
                    rateios.append(rateio)
                }
            } else {
                // Log details of the failure
                NSLog("Error: %@ %@", error, error.userInfo!)
            }
        }
        
        return rateios
    }
    
}
