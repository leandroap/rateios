//
//  NovoRateioViewController.swift
//  Rateios
//
//  Created by Leandro Pereira on 15/12/14.
//  Copyright (c) 2014 FIAP iOS Projects. All rights reserved.
//

import UIKit
import MapKit

class NovoRateioViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, LocalRateioViewControllerDelegate, AdicionarDataViewControllerDelegate, AdicionarAmigosTableViewControllerDelegate {

    @IBOutlet weak var TxtNomeDoEvento: UITextField!
    @IBOutlet weak var TxtLocalDoEvento: UITextField!
    @IBOutlet weak var txtData: UITextField!
    @IBOutlet weak var tableView: UITableView!
    
    var coordenadasRateio: CLLocationCoordinate2D!
    @IBOutlet weak var txtValor: UITextField!
    var annotationRateio: PoiAnnotation!
    var amigos:Array<Dictionary<String, AnyObject>> = Array<Dictionary<String, AnyObject>>()

    var valorEvento: Double = 0
    var facebookId: String!
    var userId: String!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        getCurrentUser()
        // Do any additional setup after loading the view.
        
        tableView.delegate = self
        tableView.dataSource = self
        
        self.facebookFriends()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func btnAddAmigo(sender: UIButton) {
        self.performSegueWithIdentifier("novoRateioToAdicionarAmigosSegue", sender: true)
    }

    @IBAction func btnInfoLocal(sender: UIButton) {
        self.performSegueWithIdentifier("novoRateioToMapKit", sender: true)
    }
    
    @IBAction func btnData(sender: UIButton)
    {
        self.performSegueWithIdentifier("SetDataToDataSegue", sender: true)
        
    }
    
    @IBAction func btnCancel(sender: UIBarButtonItem) {
        self.dismissViewControllerAnimated(true, completion: nil)
    }
    
    @IBAction func btnSave(sender: UIBarButtonItem) {
        var invalido = true
        let now = NSDate()
        
        invalido = (self.TxtNomeDoEvento.text == "" || self.coordenadasRateio == nil
            || self.txtData.text == "" || self.txtValor.text == "")
        
        if(invalido == true){
            var alert = UIAlertView(
                title: "Erro",
                message: "Preencha todos os campos",
                delegate: nil,
                cancelButtonTitle: nil,
                otherButtonTitles: "OK")
            
            alert.show()
        } else {
            
            self.valorEvento = (txtValor.text as NSString).doubleValue
            
            var novoRateio: Rateio = Rateio(nomeEvento:
                self.TxtNomeDoEvento.text as String ,
                coordinate: self.coordenadasRateio,
                dataEvento: now,
                convidados: ["facebookId": "10203688468026878"],
                valorTotal: self.valorEvento,
                userId: self.userId,
                facebookId: self.facebookId)
            
            //novoRateio.salvar()
            let convi: Convidados = Convidados(facebookId: PFUser.currentUser()["facebookId"] as! String,
                statusPgto: false,
                valorIndividual: self.valorEvento)
            
            convi.salvar(convi.toPFObject(convi), rateio: Rateio.toPFObject(novoRateio))
            
            self.dismissViewControllerAnimated(true, completion: nil)
        }
    }
    
    // MARK: - Navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {

        if(segue.identifier == "novoRateioToMapKit")
        {
            var vc:LocalRateioViewController = segue.destinationViewController as! LocalRateioViewController
            
            //LAP - Caso jah tenha escolhido o local, vamos carregar o mesmo
            if(self.annotationRateio != nil){
                vc.annotationPreviaRateio = self.annotationRateio
            }
            vc.delegado = self
            
        }
        else if(segue.identifier == "SetDataToDataSegue")
        {
            var vc:AdicionarDataViewController = segue.destinationViewController as! AdicionarDataViewController
            vc.delegate = self
            
        } else if (segue.identifier == "novoRateioToAdicionarAmigosSegue"){
            var facebookFriendsTableView:AdicionarAmigosTableViewController = segue.destinationViewController as! AdicionarAmigosTableViewController
            facebookFriendsTableView.delegate = self
        }
    }
    
    func locationAnotation(annotationTapped:PoiAnnotation){
        self.navigationController?.popViewControllerAnimated(true)
        self.annotationRateio = annotationTapped
        self.TxtLocalDoEvento.text = annotationTapped.title
        self.coordenadasRateio = annotationTapped.coordinate
    }
    
    func dataRateio(dataRateio: String)
    {
        txtData.text = dataRateio
    }
    
    func carregarAmigos(amigos:Array<Dictionary<String, AnyObject>>){
        self.amigos = amigos
    }
    
    func getCurrentUser(){
        var currentUser = PFUser.currentUser()
        if currentUser != nil {
            self.facebookId = currentUser["facebookId"] as! String
            self.userId = currentUser.objectId
        } else {
            // Show the signup or login screen
        }
    }
    
    // MARK: - Table view data source
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.amigos.count
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("AmigosId", forIndexPath: indexPath) as! UITableViewCell
        
        let dict:Dictionary<String, AnyObject> = self.amigos[indexPath.row] as Dictionary<String, AnyObject>
        cell.textLabel?.text = dict["name"]! as? String
        cell.detailTextLabel?.text = dict["id"]! as? String
        //cell.accessoryType = UITableViewCellAccessoryType.Checkmark
        //cell.imageView.image = UIImage(named: (dict["Imagem"]! as String))
        
        return cell
    }
    
    
    func tableView(tableView: UITableView, accessoryButtonTappedForRowWithIndexPath indexPath: NSIndexPath) {
        let cell = tableView.dequeueReusableCellWithIdentifier("AmigosId", forIndexPath: indexPath) as! UITableViewCell
        
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        var cell:UITableViewCell = tableView.cellForRowAtIndexPath(indexPath)!
        
        if (cell.accessoryType == UITableViewCellAccessoryType.None) {
            cell.accessoryType = UITableViewCellAccessoryType.Checkmark
        } else {
            cell.accessoryType = UITableViewCellAccessoryType.None
        }
        
        // Desmarca a celula
        tableView.deselectRowAtIndexPath(indexPath, animated: true)
    }

    func facebookFriends() {
        var friendsRequest : FBRequest = FBRequest.requestForMyFriends()
        friendsRequest.startWithCompletionHandler{(connection:FBRequestConnection!, result:AnyObject!, error:NSError!) -> Void in
            
            var errorPointer:NSErrorPointer = NSErrorPointer()
            var resultdict:Dictionary<String, AnyObject> = result as! Dictionary<String, AnyObject>
            println("Resultado: \(resultdict)")
            
            var data = resultdict["data"] as? Array<Dictionary<String, AnyObject>>
            println(data)
            
            for dict:Dictionary<String, AnyObject> in data! {
                let id = dict["id"] as! String
                println("id: \(id)")
            }
            
            self.amigos = resultdict["data"] as! Array<Dictionary<String, AnyObject>>!
            println("Amigos usando o App: \(self.amigos.count) ")
            self.tableView.reloadData()
        }
    }
}
