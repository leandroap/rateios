//
//  DevendoTableViewController.swift
//  Rateios
//
//  Created by Mozart Falcão on 01/11/14.
//  Copyright (c) 2014 FIAP iOS Projects. All rights reserved.
//

import UIKit

class DevendoTableViewController: UITableViewController {

    var facebookId: String!
    var userId: String!
    var rateiosDevendo:Array<Rateio> = Array<Rateio>()

    lazy var dateFormatter: NSDateFormatter = {
    let dateFormatter = NSDateFormatter()
        dateFormatter.dateStyle = .MediumStyle
        dateFormatter.timeStyle = .ShortStyle
    return dateFormatter
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        //LAP20141105 - Pegar o usuario ativo e os seus rateios
        self.findDevendo()
        
        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem()
    }

    override func viewDidAppear(animated: Bool) {
        //LAP20141109 - Atualiza a tela
        tableView.reloadData()
        super.viewDidAppear(animated)
        println("View carregada na tela")
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        println("View ficara visivel")
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func findDevendo(){
        var query:PFQuery = PFQuery(className:"guests")
        query.whereKey("facebookId", equalTo: PFUser.currentUser()["facebookId"])
        query.whereKey("paid", equalTo: false)
        
        query.findObjectsInBackgroundWithBlock {
            (objects: [AnyObject]!, error: NSError!) -> Void in
            if error == nil {
                // The find succeeded.
                if (objects.count > 0){
                    self.rateiosDevendo.removeAll()
                }
                NSLog("Rateios retrieved: \(objects.count)")
                for object in objects {
                    NSLog("%@", object.objectId)
                    let apportionment:PFObject = object["parent"] as! PFObject
                    apportionment.fetchIfNeededInBackgroundWithBlock {
                        (object: PFObject!, error: NSError!) -> Void in
                        self.rateiosDevendo.append(Rateio(apportionment: (object as! Apportionment)))
                    }
                }
                self.tableView.reloadData()
            } else {
                // Log details of the failure
                NSLog("Error: %@ %@", error, error.userInfo!)
            }
            self.tableView.reloadData()
        }
        
    }
    
    // MARK: - Table view data source

    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        // #warning Potentially incomplete method implementation.
        // Return the number of sections.
        return 1
    }

    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete method implementation.
        // Return the number of rows in the section.
        return self.rateiosDevendo.count
    }

    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("CellId", forIndexPath: indexPath) as! UITableViewCell

        let rateio:Rateio = self.rateiosDevendo[indexPath.row]
        cell.textLabel?.text = rateio.nomeEvento
        cell.detailTextLabel?.text = dateFormatter.stringFromDate(rateio.dataEvento)

        return cell
    }

    /*
    // Override to support conditional editing of the table view.
    override func tableView(tableView: UITableView, canEditRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        // Return NO if you do not want the specified item to be editable.
        return true
    }
    */

    /*
    // Override to support editing the table view.
    override func tableView(tableView: UITableView, commitEditingStyle editingStyle: UITableViewCellEditingStyle, forRowAtIndexPath indexPath: NSIndexPath) {
        if editingStyle == .Delete {
            // Delete the row from the data source
            tableView.deleteRowsAtIndexPaths([indexPath], withRowAnimation: .Fade)
        } else if editingStyle == .Insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(tableView: UITableView, moveRowAtIndexPath fromIndexPath: NSIndexPath, toIndexPath: NSIndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(tableView: UITableView, canMoveRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        // Return NO if you do not want the item to be re-orderable.
        return true
    }
    */

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using [segue destinationViewController].
        // Pass the selected object to the new view controller.
    }
    */

}
