//
//  AdicionarDataViewController.swift
//  Rateios
//
//  Created by Mozart Falcão on 01/11/14.
//  Copyright (c) 2014 FIAP iOS Projects. All rights reserved.
//

import UIKit

protocol AdicionarDataViewControllerDelegate: class {
    
    func dataRateio(dataRateio: String)
    
}
class AdicionarDataViewController: UIViewController {

    internal weak var delegate:AdicionarDataViewControllerDelegate?
   
    
    @IBOutlet weak var datePicker: UIDatePicker!
    
    @IBOutlet weak var dateLabel: UILabel!
    
    
    lazy var dateFormatter: NSDateFormatter = {
        let dateFormatter = NSDateFormatter()
        
        dateFormatter.dateStyle = .MediumStyle
        dateFormatter.timeStyle = .ShortStyle
        
        return dateFormatter
        }()
    
    
    @IBAction func btnCancelar(sender: UIButton)
    {
        self.dismissViewControllerAnimated(true, completion: nil)
    }
    
    
    @IBAction func btnOK(sender: UIButton)
    {
        dataRateio(dateLabel.text!)
        self.dismissViewControllerAnimated(true, completion: nil)
    }
    
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
       
        configureDatePicker()   
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: Configuration
    
    func configureDatePicker() {
        datePicker.datePickerMode = .DateAndTime
        
        // Set min/max date for the date picker.
        // As an example we will limit the date between now and 7 days from now.
        let now = NSDate()
        datePicker.minimumDate = now
        
        let currentCalendar = NSCalendar.currentCalendar()
        
        datePicker.minuteInterval = 2
        
        datePicker.addTarget(self, action: "updateDatePickerLabel", forControlEvents: .ValueChanged)
        
        updateDatePickerLabel()
    }
    
    // MARK: Actions
    
    func updateDatePickerLabel() {
        dateLabel.text = dateFormatter.stringFromDate(datePicker.date)
    }
    
    func dataRateio(dataRateio: String)
    {
        self.delegate?.dataRateio(dataRateio)
    }
    
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
