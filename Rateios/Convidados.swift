//
//  Convidados.swift
//  Rateios
//
//  Created by Leandro Pereira on 05/11/14.
//  Copyright (c) 2014 FIAP iOS Projects. All rights reserved.
//

import UIKit

class Convidados: NSObject {
   let CLASSNAME: String = "guests"
    //var rateioId: String
    var facebookId: String
    var statusPgto: Bool
    var valorIndividual: Double
    
    init(facebookId: String, statusPgto: Bool, valorIndividual: Double){
        //self.rateioId = rateioId
        self.facebookId = facebookId
        self.statusPgto = statusPgto
        self.valorIndividual = valorIndividual
    }
    
    init(guests: Guests){
        //self.rateioId = guests.rateioId()
        self.facebookId = guests.facebookId()
        self.statusPgto = guests.statusPgto()
        self.valorIndividual = guests.valorIndividual()
    }
    
    func toPFObject(valores: Convidados) -> PFObject{
        var pfObject = PFObject(className: CLASSNAME)
        
        //pfObject["apportionmentId"] = valores.rateioId
        pfObject["facebookId"] = valores.facebookId
        pfObject["paid"] = valores.statusPgto
        pfObject["value"] = valores.valorIndividual
        
        return pfObject
    }
    
    func salvar(convidados: PFObject, rateio: PFObject){
        //var pfObject = self.toPFObject(self)
        //pfObject.saveEventually()
        
        convidados["parent"] = rateio
        convidados.saveEventually()
    }
}
