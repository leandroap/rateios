//
//  DetalhesRateioViewController.swift
//  Rateios
//
//  Created by Mozart Falcão on 23/10/14.
//  Copyright (c) 2014 FIAP iOS Projects. All rights reserved.
//

import UIKit


class DetalhesRateioViewController: UIViewController {
    
    var rateio:Rateio?
    var statusPagamento: Bool?
    
    lazy var dateFormatter: NSDateFormatter = {
        let dateFormatter = NSDateFormatter()
        
        dateFormatter.dateStyle = .MediumStyle
        dateFormatter.timeStyle = .ShortStyle
        
        return dateFormatter
        }()
    
     //****** OUTLETS **********
    
    @IBOutlet weak var txtNomeDoEvento: UITextField!

    @IBOutlet weak var txtLocal: UITextField!

    @IBOutlet weak var txtData: UITextField!
    
     //****** OUTLETS FIM **********
    
    
     //****** Métodos Botões **********
    
    
    @IBAction func btnLocal(sender: UIButton)
    {
        
        self.performSegueWithIdentifier("LocationDetailtoMap", sender: nil)

    }
    
    @IBAction func btnOk(sender: UIButton)
    {
//        if (self.rateio!.paid != self.statusPagamento){
//            self.rateio!.paid = self.statusPagamento!
//            self.rateio?.salvar()
//        }
        self.dismissViewControllerAnimated(true, completion: nil)
    }
    
    
    @IBAction func btnSwich(sender: UISwitch)
    {
        self.statusPagamento = sender.on
    }
    
     //****** Métodos Botões FIM **********
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        txtNomeDoEvento.text = self.rateio!.nomeEvento
        txtData.text = dateFormatter.stringFromDate(self.rateio!.dataEvento)

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?)
    {
    
        if (segue.identifier == "LocationDetailtoMap")
        {
            var vc:DetalheLocalViewController = segue.destinationViewController as! DetalheLocalViewController
            //            //vc.delegate = self
            //            //let cell = sender as UITableViewCell
            vc.rateio = self.rateio
            
        }
    
    }
    
}
