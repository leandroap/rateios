//
//  ViewController.swift
//  Rateios
//
//  Created by Mozart Falcao on 21/10/14.
//  Copyright (c) 2014 FIAP iOS Projects. All rights reserved.
//

import UIKit

class ViewController: UIViewController, UITextFieldDelegate, FBLoginViewDelegate, FBViewControllerDelegate {

    @IBOutlet weak var fbLoginView: FBLoginView!
    
    var pUser = PFUser()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


    
    func loginViewShowingLoggedInUser(loginView: FBLoginView!) {
        println("User Logged In")
    }
    
    func loginViewFetchedUserInfo(loginView: FBLoginView!, user: FBGraphUser!) {
        println("User: \(user)")
        println("User ID: \(user.objectID)")
        println("User Name: \(user.name)")
        var userEmail = user.objectForKey("email") as? String
        println("User Email: \(userEmail)")
        var userName = user.first_name + "." + user.last_name
        
        println("userName: \(userName)")
        
        //var pUser = PFUser()
        self.pUser.username = userName.lowercaseString
        self.pUser.password = user.objectID
        self.pUser.email = userEmail
        self.pUser.setValue(user.objectID, forKey: "facebookId")
        
        self.parseLogin(self.pUser)
    }
    
    func loginViewShowingLoggedOutUser(loginView: FBLoginView!) {
        println("User Logged Out")
    }
    
    func loginView(loginView: FBLoginView!, handleError: NSError!) {
        println("Error: \(handleError.localizedDescription)")
    }
    
    func parseSingin(user: PFUser){
        
        user.signUpInBackgroundWithBlock {
            (succeeded: Bool, error: NSError?) -> Void in
            if error == nil {
                println("Funcionou!")
                 self.performSegueWithIdentifier("LogonWindowToTelaPrincipal", sender: nil)
            } else {
                let errorString: String = error!.userInfo?["error"] as! String
                println("Error: \(errorString)")
            }
        }
    }
    
    func parseLogin(user: PFUser){
        PFUser.logInWithUsernameInBackground(user.username, password: user.password) {
            (user: PFUser!, error: NSError!) -> Void in
            if user != nil {
                println("Caiu na Rede!")
                 self.performSegueWithIdentifier("LogonWindowToTelaPrincipal", sender: nil)
            } else {
                let errorString: String = error.userInfo?["error"] as! String
                println("Error: \(errorString)")
                
                self.parseSingin(self.pUser)
            }
        }
    }
    
//    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject!) {
//        
//        if (segue.identifier == "LogonWindowToTelaPrincipal"){
//            var vc:MeusRateiosViewController = segue.destinationViewController as MeusRateiosViewController
//            vc.facebookId = self.pUser["facebookId"] as String
//        }
//    }
    
}

