//
//  MeusRateiosTableViewController.swift
//  Rateios
//
//  Created by Mozart Falcão on 08/11/14.
//  Copyright (c) 2014 FIAP iOS Projects. All rights reserved.
//

import UIKit

class MeusRateiosTableViewController: UITableViewController, NovoRateioTableViewControllerDelegate{
    
    var facebookId: String!
    var userId: String!
    var meusRateios:Array<Rateio> = Array<Rateio>()
    var fbFriends: Array<Dictionary<String, AnyObject>> = Array<Dictionary<String, AnyObject>>()
    
    
    lazy var dateFormatter: NSDateFormatter = {
        let dateFormatter = NSDateFormatter()
        
        dateFormatter.dateStyle = .MediumStyle
        dateFormatter.timeStyle = .ShortStyle
        
        return dateFormatter
        }()
    
    @IBAction func btnNovoRateio(sender: UIBarButtonItem){
        self.performSegueWithIdentifier("MeusRateiosToNovoRateiosSegue", sender: nil)
    }
    
    
    @IBAction func btnLogout(sender: UIBarButtonItem)
    {
        self.dismissViewControllerAnimated(true, completion: nil)

    }
    
    func findMyRateios()
    {
        
        var query:PFQuery = PFQuery(className:"apportionment")
        query.whereKey("userId", equalTo: PFUser.currentUser().objectId)
        query.findObjectsInBackgroundWithBlock {
            (objects: [AnyObject]!, error: NSError!) -> Void in
            
            if error == nil {
                // The find succeeded.
                if (objects.count > 0){
                    self.meusRateios.removeAll()
                }
                NSLog("Rateios retrieved: \(objects.count)")
                for object in objects {
                    println(object)
                    NSLog("%@", object.objectId)
                    let rateio:Apportionment = object as! Apportionment
                 
                    self.meusRateios.append(Rateio(apportionment: rateio))
                }
                
                self.tableView.reloadData()

            } else {
                // Log details of the failure
                NSLog("Error: %@ %@", error, error.userInfo!)
            }
            
            self.tableView.reloadData()
        }
        
    }
    
    func atualizaTable()
    {
        
        self.meusRateios = []
        self.tableView.reloadData()
        self.findMyRateios()
        self.tableView.reloadData()
        viewDidLoad()
    }
    
    override func viewDidAppear(animated: Bool) {
        //LAP20141109 - Atualiza a tela
        self.findMyRateios()
        tableView.reloadData()
        super.viewDidAppear(animated)
        println("View carregada na tela")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false
        
        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem()
        
        //LAP20141108 - Buscar os Rateios
        self.findMyRateios()
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - Table view data source
    
    
    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        // #warning Potentially incomplete method implementation.
        // Return the number of sections.
        return 1
    }
    
    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete method implementation.
        // Return the number of rows in the section.
        return self.meusRateios.count
    }
    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("CellId", forIndexPath: indexPath) as! UITableViewCell
        
        let rateio:Rateio = self.meusRateios[indexPath.row]
        cell.textLabel?.text = rateio.nomeEvento
        cell.detailTextLabel?.text = dateFormatter.stringFromDate(rateio.dataEvento)
        //        let str:String? = dict["imagem"]! as? String
        //        let imgData:NSData = NSData.dataWithContentsOfURL(NSURL(string: str!), options: nil, error: nil)
        //        cell.imageView?.image = UIImage(data: imgData)
        return cell
    }
   
    
    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath)
    {
        self.performSegueWithIdentifier("MeusRateiosToDetalhes", sender: indexPath)
    }
    
    
    /*
    // Override to support conditional editing of the table view.
    override func tableView(tableView: UITableView, canEditRowAtIndexPath indexPath: NSIndexPath) -> Bool {
    // Return NO if you do not want the specified item to be editable.
    return true
    }
    */
    
    /*
    // Override to support editing the table view.
    override func tableView(tableView: UITableView, commitEditingStyle editingStyle: UITableViewCellEditingStyle, forRowAtIndexPath indexPath: NSIndexPath) {
    if editingStyle == .Delete {
    // Delete the row from the data source
    tableView.deleteRowsAtIndexPaths([indexPath], withRowAnimation: .Fade)
    } else if editingStyle == .Insert {
    // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }
    }
    */
    
    /*
    // Override to support rearranging the table view.
    override func tableView(tableView: UITableView, moveRowAtIndexPath fromIndexPath: NSIndexPath, toIndexPath: NSIndexPath) {
    
    }
    */
    
    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(tableView: UITableView, canMoveRowAtIndexPath indexPath: NSIndexPath) -> Bool {
    // Return NO if you do not want the item to be re-orderable.
    return true
    }
    */
    
    /*
    override func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
    return 160.0
    }
    */
    
    // MARK: - Navigation
    
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if (segue.identifier == "MeusRateiosToNovoRateiosSegue"){
            
            

        }
        if (segue.identifier == "MeusRateiosToDetalhes")
        {
            var vc:DetalhesRateioViewController = segue.destinationViewController as! DetalhesRateioViewController
//            //vc.delegate = self
            let indexPath: AnyObject? = sender
            vc.rateio = self.meusRateios[indexPath!.row]
        }

    }
    
}
