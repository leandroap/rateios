//
//  NovoRateioTableViewController.swift
//  Rateios
//
//  Created by Mozart Falcão on 01/11/14.
//  Copyright (c) 2014 FIAP iOS Projects. All rights reserved.
//

import UIKit
import AddressBook

protocol NovoRateioTableViewControllerDelegate: class {
    
    func atualizaTable()
    
}

class NovoRateioTableViewController: UITableViewController{
   
    var facebookId: String!
    var userId: String!
    
    var fbAmigos:Array<Dictionary<String, AnyObject>> = Array<Dictionary<String, AnyObject>>()
    var convidados:Dictionary<String, AnyObject>?
    
     internal weak var delegate:NovoRateioTableViewControllerDelegate?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem()
        self.getCurrentUser()
        println("TEsteeeee")
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - Table view data source
    func facebookFriends() -> Array<Dictionary<String, AnyObject>>! {
        var friendsRequest : FBRequest = FBRequest.requestForMyFriends()
        var friends: Array<Dictionary<String, AnyObject>> = Array<Dictionary<String, AnyObject>>()
        friendsRequest.startWithCompletionHandler{(connection:FBRequestConnection!, result:AnyObject!, error:NSError!) -> Void in
            //var resultdict = result as NSDictionary
            
            var errorPointer:NSErrorPointer = NSErrorPointer()
            var resultdict:Dictionary<String, AnyObject> = result as! Dictionary<String, AnyObject>
            println("Result Dict: \(resultdict)")
            
            //var data : NSArray = resultdict.objectForKey("data") as NSArray
            var data = resultdict["data"] as? Array<Dictionary<String, AnyObject>>
            println(data)
            
            for dict:Dictionary<String, AnyObject> in data! {
                let id = dict["id"] as! String
                println("the id value is \(id)")
            }
            
            var friends = resultdict["data"] as? Array<Dictionary<String, AnyObject>>
            println("Found \(friends?.count) friends")
            self.fbAmigos = friends!
        }
        
        return friends
    }
  
    
//    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
//        // #warning Potentially incomplete method implementation.
//        // Return the number of sections.
//        return 0
//    }
//
//    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
//        // #warning Incomplete method implementation.
//        // Return the number of rows in the section.
//        return 0
//    }

    /*
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("reuseIdentifier", forIndexPath: indexPath) as UITableViewCell

        // Configure the cell...

        return cell
    }
    */

    /*
    // Override to support conditional editing of the table view.
    override func tableView(tableView: UITableView, canEditRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        // Return NO if you do not want the specified item to be editable.
        return true
    }
    */

    /*
    // Override to support editing the table view.
    override func tableView(tableView: UITableView, commitEditingStyle editingStyle: UITableViewCellEditingStyle, forRowAtIndexPath indexPath: NSIndexPath) {
        if editingStyle == .Delete {
            // Delete the row from the data source
            tableView.deleteRowsAtIndexPaths([indexPath], withRowAnimation: .Fade)
        } else if editingStyle == .Insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(tableView: UITableView, moveRowAtIndexPath fromIndexPath: NSIndexPath, toIndexPath: NSIndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(tableView: UITableView, canMoveRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        // Return NO if you do not want the item to be re-orderable.
        return true
    }
    */

    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    /*
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?)
    {

    }
    */
    
    func carregarAmigos(amigos:Array<Dictionary<String, AnyObject>>){
        
    }
    
    func getCurrentUser(){
        var currentUser = PFUser.currentUser()
        if currentUser != nil {
            self.facebookId = currentUser["facebookId"] as! String
            self.userId = currentUser.objectId
        } else {
            // Show the signup or login screen
        }
    }
    
    func atualizaTable()
    {
        self.delegate?.atualizaTable()
    }
    
}
