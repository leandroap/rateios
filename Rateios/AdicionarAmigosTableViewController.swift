//
//  AdicionarAmigosTableViewController.swift
//  Rateios
//
//  Created by Mozart Falcão on 01/11/14.
//  Copyright (c) 2014 FIAP iOS Projects. All rights reserved.
//

import UIKit

protocol AdicionarAmigosTableViewControllerDelegate: class {
    
    func carregarAmigos(amigos:Array<Dictionary<String, AnyObject>>)
    
}
class AdicionarAmigosTableViewController: UITableViewController {

    internal weak var delegate:AdicionarAmigosTableViewControllerDelegate?
    
    var amigos:Array<Dictionary<String, AnyObject>> = Array<Dictionary<String, AnyObject>>()
    var selecionados:Array<Dictionary<String, AnyObject>> = Array<Dictionary<String, AnyObject>>()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Uncomment the following line to preserve selection between presentations
        //self.clearsSelectionOnViewWillAppear = false

        self.facebookFriends()
        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        //self.navigationItem.rightBarButtonItem = self.editButtonItem()
    }
    
    func facebookFriends() {
        var friendsRequest : FBRequest = FBRequest.requestForMyFriends()
        friendsRequest.startWithCompletionHandler{(connection:FBRequestConnection!, result:AnyObject!, error:NSError!) -> Void in
            
            var errorPointer:NSErrorPointer = NSErrorPointer()
            var resultdict:Dictionary<String, AnyObject> = result as! Dictionary<String, AnyObject>
            println("Resultado: \(resultdict)")
        
            var data = resultdict["data"] as? Array<Dictionary<String, AnyObject>>
            println(data)
            
            for dict:Dictionary<String, AnyObject> in data! {
                let id = dict["id"] as! String
                println("id: \(id)")
            }
            
            self.amigos = resultdict["data"] as! Array<Dictionary<String, AnyObject>>!
            println("Amigos usando o App: \(self.amigos.count) ")
            self.tableView.reloadData()
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    // MARK: - Table view data source

    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }

    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.amigos.count
    }

    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("AmigosId", forIndexPath: indexPath) as! UITableViewCell

        let dict:Dictionary<String, AnyObject> = self.amigos[indexPath.row] as Dictionary<String, AnyObject>
        cell.textLabel?.text = dict["name"]! as? String
        cell.detailTextLabel?.text = dict["id"]! as? String
        //cell.accessoryType = UITableViewCellAccessoryType.Checkmark
        //cell.imageView.image = UIImage(named: (dict["Imagem"]! as String))

        return cell
    }
    
    
    override func tableView(tableView: UITableView, accessoryButtonTappedForRowWithIndexPath indexPath: NSIndexPath) {
        let cell = tableView.dequeueReusableCellWithIdentifier("AmigosId", forIndexPath: indexPath) as! UITableViewCell
        
    }

    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        var cell:UITableViewCell = tableView.cellForRowAtIndexPath(indexPath)!
        
        if (cell.accessoryType == UITableViewCellAccessoryType.None) {
            cell.accessoryType = UITableViewCellAccessoryType.Checkmark
        } else {
            cell.accessoryType = UITableViewCellAccessoryType.None
        }
        
        // Desmarca a celula
        tableView.deselectRowAtIndexPath(indexPath, animated: true)
    }

    /*
    // Override to support conditional editing of the table view.
    override func tableView(tableView: UITableView, canEditRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        // Return NO if you do not want the specified item to be editable.
        return true
    }
    */

    /*
    // Override to support editing the table view.
    override func tableView(tableView: UITableView, commitEditingStyle editingStyle: UITableViewCellEditingStyle, forRowAtIndexPath indexPath: NSIndexPath) {
        if editingStyle == .Delete {
            // Delete the row from the data source
            tableView.deleteRowsAtIndexPaths([indexPath], withRowAnimation: .Fade)
        } else if editingStyle == .Insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(tableView: UITableView, moveRowAtIndexPath fromIndexPath: NSIndexPath, toIndexPath: NSIndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(tableView: UITableView, canMoveRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        // Return NO if you do not want the item to be re-orderable.
        return true
    }
    */

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using [segue destinationViewController].
        // Pass the selected object to the new view controller.
    }
    */

}
