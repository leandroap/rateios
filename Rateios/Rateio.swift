//
//  Rateio.swift
//  Rateios
//
//  Created by Leandro Pereira on 05/11/14.
//  Copyright (c) 2014 FIAP iOS Projects. All rights reserved.
//

import UIKit

class Rateio: NSObject {
   
    let CLASSNAME: String = "apportionment"
    var nomeEvento: String
    var coordinate: CLLocationCoordinate2D
    var dataEvento: NSDate
    var convidados: Dictionary<String, AnyObject>
    var valorTotal: Double
    var userId: String
    var facebookId: String
    
    init(nomeEvento: String, coordinate: CLLocationCoordinate2D,
        dataEvento: NSDate, convidados: Dictionary<String, AnyObject>,
        valorTotal: Double, userId: String, facebookId: String) {
            
        self.nomeEvento = nomeEvento
        self.coordinate = coordinate
        self.dataEvento = dataEvento
        self.convidados = convidados
        self.valorTotal = valorTotal
        self.userId = userId
        self.facebookId = facebookId
    }
    
    init(apportionment: Apportionment){
        self.nomeEvento = apportionment.nomeEvento()
        self.coordinate = apportionment.coordinate()
        self.dataEvento = apportionment.dataEvento()
        self.convidados = apportionment.convidados()
        self.valorTotal = apportionment.valorTotal()
        self.userId = apportionment.userId()
        self.facebookId = apportionment.facebookId()
    }
    
    class func toPFObject(valores: Rateio) -> PFObject{
        
        var pfObject = PFObject(className: valores.CLASSNAME)
        
        pfObject["title"] = (valores.nomeEvento == "" ? "" : valores.nomeEvento)
        pfObject["latitude"] = valores.coordinate.latitude
        pfObject["longitude"] = valores.coordinate.longitude
        pfObject["date"] = valores.dataEvento
        pfObject["convidados"] = (valores.convidados.isEmpty ? nil : valores.convidados)
        pfObject["totalValue"] = (valores.valorTotal == 0 ? 0 : valores.valorTotal)
        pfObject["userId"] = (valores.userId == "" ? "" : valores.userId)
        pfObject["facebookId"] = (valores.facebookId == "" ? "" : valores.facebookId)
        
        return pfObject
    }
    
    class func toRateio(pfObject: PFObject) -> Rateio{
        
        var valores:Rateio = Rateio(
        nomeEvento: pfObject["title"] as! String,
        coordinate: CLLocationCoordinate2DMake(
            pfObject["latitude"] as! CLLocationDegrees,
            pfObject["longitude"] as! CLLocationDegrees),
        dataEvento: pfObject["date"] as! NSDate,
        convidados: pfObject["convidados"] as! Dictionary<String, AnyObject>,
        valorTotal: pfObject["totalValue"] as! Double,
        userId: pfObject["userId"] as! String,
        facebookId: pfObject["facebookId"] as! String)
        
        return valores
    }
    
    func salvar() -> PFObject{
        var pfObject = Rateio.toPFObject(self)
        pfObject.saveEventually()
        return pfObject
    }
    
    func findById(){
        
    }
    
    func findByUserId(userId: String){
        let predicate = NSPredicate(format:"userId = '\(userId)'")
        var query = PFQuery(className: self.CLASSNAME, predicate:predicate)
        
        query.findObjectsInBackgroundWithBlock {
            (objects: [AnyObject]!, error: NSError!) -> Void in
            if error == nil {
                // The find succeeded.
                NSLog("Rateios retrieved: \(objects.count)")
                // Do something with the found objects
                for object in objects {
                    NSLog("%@", object.objectId)
                }
            } else {
                // Log details of the failure
                NSLog("Error: %@ %@", error, error.userInfo!)
            }
        }
    }
}
