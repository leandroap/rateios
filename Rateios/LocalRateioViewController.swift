//
//  LocalRateioViewController.swift
//  Rateios
//
//  Created by Usuário Convidado on 27/10/14.
//  Copyright (c) 2014 FIAP iOS Projects. All rights reserved.
//

import UIKit
import MapKit

protocol LocalRateioViewControllerDelegate: class {
    func locationAnotation(annotationTapped:PoiAnnotation)
}


class LocalRateioViewController: UIViewController, MKMapViewDelegate, UISearchBarDelegate {
    
    internal weak var delegado:LocalRateioViewControllerDelegate?
    
    var locationManager: CLLocationManager = CLLocationManager()
    var annotationPreviaRateio: PoiAnnotation!
    
    @IBOutlet weak var mapView: MKMapView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //LAP20141105
        //Cria uma localizacao
        let userLocation:CLLocationCoordinate2D = CLLocationCoordinate2DMake(-23.550303,-46.634184)
        
        
        self.locationManager.requestAlwaysAuthorization()
        self.locationManager.requestWhenInUseAuthorization()
        self.mapView.showsUserLocation = true
        
    
        //Define o tamanho da visao que vou mostrar
        self.mapView.region = MKCoordinateRegionMakeWithDistance(userLocation, 12000, 12000)
        
        
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    
    @IBAction func changeMapType(sender: UISegmentedControl)
    {
        switch sender.selectedSegmentIndex
        {
        case 0:
            self.mapView.mapType = MKMapType.Standard
        case 1:
            self.mapView.mapType = MKMapType.Satellite
            
        case 2:
            self.mapView.mapType = MKMapType.Hybrid
        default:
            println("default")
        }
        
    }
    
    
    func mapView(mapView: MKMapView!, viewForAnnotation annotation: MKAnnotation!) -> MKAnnotationView! {
        
        
        if annotation is PoiAnnotation{
            
            //verificar se a marcação já existe para tentar reutilizá-la
            let reuseId = "PoiAnnotation"
            var anView = mapView.dequeueReusableAnnotationViewWithIdentifier(reuseId)
            
            //se a view não existir
            if anView == nil
            {
                //criar a view como subclasse de MKAnnotationView
                anView = MKAnnotationView(annotation: annotation, reuseIdentifier: reuseId)
                anView.image =  UIImage(named: "simplePin")
                // anView.image = nil
                
                //permitir que mostre o "balão" com informações da marcação
                anView.canShowCallout = true
                
                //adiciona um botão do lado direito do balão para futuro 'tap'
                anView.rightCalloutAccessoryView = UIButton.buttonWithType(UIButtonType.DetailDisclosure) as! UIButton
                
                //LAP20141105 - Regiao do Ponto Localizado
                self.mapView.region = MKCoordinateRegionMakeWithDistance(annotation.coordinate, 8000, 8000)
                
                
            }
            return anView
        }
        else
        {
            let reuseId = "PoiAnnotation"
            var anView = mapView.dequeueReusableAnnotationViewWithIdentifier(reuseId)
            
            if anView == nil
            {
                //criar a view como subclasse de MKAnnotationView
                anView = MKAnnotationView(annotation: annotation, reuseIdentifier: reuseId)
                
                anView.image = nil
                
                
            }
            return anView
        }
        
    }
    
    
    func mapView(mapView: MKMapView!, annotationView view: MKAnnotationView!, calloutAccessoryControlTapped control: UIControl!){
        let poiAnnotation = view.annotation as! PoiAnnotation
        locationAnotation(poiAnnotation)
    }
    
    func searchBarSearchButtonClicked(searchBar: UISearchBar)
    {
        var request = MKLocalSearchRequest()
        request.naturalLanguageQuery = searchBar.text
        request.region = self.mapView.region
        
        var search = MKLocalSearch(request: request)
        
        var loading = UIActivityIndicatorView(activityIndicatorStyle: UIActivityIndicatorViewStyle.Gray)
        loading.center = self.view.center
        self.view.addSubview(loading)
        loading.startAnimating()
        
        
        //inicia busca
        search.startWithCompletionHandler
            {
                (response: MKLocalSearchResponse!, erro: NSError!) -> Void in
                
                
                if (response == nil)
                {
                    
                    let alert = UIAlertView()
                    alert.title = "Atenção!"
                    alert.message = "Localizacao nao encontrada"
                    alert.addButtonWithTitle("OK")
                    alert.show()
                    loading.stopAnimating()
                    searchBar.resignFirstResponder()
                    
                }
                else
                {
                    if erro == nil
                    {
                        //criar um array para guardar os resultados retornados
                        var placemarks = NSMutableArray()
                        
                        
                        for item in response.mapItems{
                            
                            let place = PoiAnnotation(coordinate: (item as! MKMapItem).placemark.coordinate, title: (item as! MKMapItem).name, subtitle: "", mapitens: (item as! MKMapItem))
                            
                            placemarks.addObject(place)
                        }
                        self.mapView.removeAnnotations(self.mapView.annotations)
                        self.mapView.addAnnotations(placemarks as [AnyObject])
                    }
                    
                    loading.stopAnimating()
                    searchBar.resignFirstResponder()
                    
                }
                
            
        }
        
    }
    
    
    func locationAnotation(annotationTapped:PoiAnnotation){
        self.delegado?.locationAnotation(annotationTapped)
        
    }
    
}
