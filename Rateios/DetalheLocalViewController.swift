//
//  DetalheLocalViewController.swift
//  Rateios
//
//  Created by Mozart Falcão on 29/12/14.
//  Copyright (c) 2014 FIAP iOS Projects. All rights reserved.
//

import UIKit
import MapKit

class DetalheLocalViewController: UIViewController, MKMapViewDelegate {

    var locationManager: CLLocationManager = CLLocationManager()
    var rateio:Rateio?
    

    
    @IBOutlet weak var mapView: MKMapView!
    

    @IBAction func btnOk(sender: AnyObject)
    {
         self.dismissViewControllerAnimated(true, completion: nil)
        
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        let userLocation:CLLocationCoordinate2D = CLLocationCoordinate2DMake(self.rateio!.coordinate.latitude, self.rateio!.coordinate.longitude)
        
        
        self.mapView.region = MKCoordinateRegionMakeWithDistance(userLocation, 800, 800)
        
        let myAnnotation:MKPointAnnotation = MKPointAnnotation()
        myAnnotation.coordinate = self.rateio!.coordinate
        myAnnotation.title = self.rateio!.nomeEvento

        self.mapView.addAnnotations([myAnnotation])
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
