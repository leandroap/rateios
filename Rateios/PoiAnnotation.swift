//
//  PoiAnnotation.swift
//  Rateios
//
//  Created by Usuário Convidado on 27/10/14.
//  Copyright (c) 2014 FIAP iOS Projects. All rights reserved.
//

import UIKit
import MapKit

class PoiAnnotation: NSObject, MKAnnotation
{

    var coordinate: CLLocationCoordinate2D
    var title: String
    var subtitle: String
    var mapitens : MKMapItem
    var imageName = UIImage(named: "simplePin")
    
    init(coordinate: CLLocationCoordinate2D, title: String, subtitle: String, mapitens: MKMapItem)
    {
        self.coordinate = coordinate
        self.title = title
        self.subtitle = subtitle
        self.mapitens = mapitens
    }

    
   
}
