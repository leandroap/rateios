//
//  Apportionment.swift
//  Rateios
//
//  Created by Leandro Pereira on 08/11/14.
//  Copyright (c) 2014 FIAP iOS Projects. All rights reserved.
//

import UIKit

class Apportionment: PFObject, PFSubclassing {
    
    override class func initialize() {
        struct Static {
            static var onceToken : dispatch_once_t = 0;
        }
        dispatch_once(&Static.onceToken) {
            self.registerSubclass()
        }
    }
    
    class func parseClassName() -> String! {
        return "apportionment"
    }
    
    func nomeEvento() -> String {
        return self["title"] as! String
    }
    
    func coordinate() -> CLLocationCoordinate2D {
        return CLLocationCoordinate2DMake(
            self["latitude"] as! CLLocationDegrees,
            self["longitude"] as! CLLocationDegrees)
    }
    
    func dataEvento() -> NSDate {
        return self["date"] as! NSDate
    }
    
    func convidados() -> Dictionary<String, AnyObject> {
        return self["convidados"] as! Dictionary<String, AnyObject>
    }
    
    func valorTotal() -> Double {
        return self["totalValue"] as! Double
    }
    
    func userId() -> String {
        return self["userId"] as! String
    }
    
    func facebookId() -> String {
        return self["facebookId"] as! String
    }
    
    class func findByUserId() -> Array<Rateio>! {
        //let predicate = NSPredicate(format:"userId = '\(userId)'")
        //var query = PFQuery(className: "apportionment", predicate:predicate)
        
        var rateios:Array<Rateio> = Array<Rateio>()
        
        let query:PFQuery = PFQuery(className: "apportionment")//self.query()
        query.whereKey("userId", equalTo: PFUser.currentUser().objectId)
        
        query.findObjectsInBackgroundWithBlock {
            (objects: [AnyObject]!, error: NSError!) -> Void in
            if error == nil {
                // The find succeeded.
                NSLog("Rateios retrieved: \(objects.count)")
                // Do something with the found objects
                for object in objects {
                    NSLog("%@", object.objectId)
                    let rateio:Rateio = Rateio(apportionment: object as! Apportionment)
                    rateios.append(rateio)
                }
            } else {
                // Log details of the failure
                NSLog("Error: %@ %@", error, error.userInfo!)
            }
        }
        
        return rateios
    }
    
    class func findById(objectId: String){
        let query:PFQuery = self.query()
        query.getObjectInBackgroundWithId(objectId) {
            (apportionment: PFObject!, error: NSError!) -> Void in
            if error == nil {
                NSLog("%@", apportionment)
            } else {
                NSLog("%@", error)
            }
        }
    }
}
